import axios from "axios";

const state = {
  list: [],
};

//to handle state
const getters = {};

//to handle actions
const actions = {
  getList({ commit }) {
    axios.get("http://demo2725834.mockable.io/api/v1/list").then((response) => {
      commit("GET_LIST", response.data);
    });
  },
};

//to handle mutations
const mutations = {
  GET_LIST(state, res) {
    state.list = res.data;
  },
};

//export store module
export default {
  namespaces: true,
  state,
  getters,
  actions,
  mutations,
};
